CC=g++
CFLAGS=`pkg-config --cflags opencv` -std=c++11
LDFLAGS=`pkg-config --libs opencv`
SRC=$(wildcard *.cpp)

all: calc-area

calc-area: hough-lines.o merge-rows.o sample-area.o main.o
	$(CC) -o $@ $^ $(CFLAGS) $(LDFLAGS)

doc: Doxyfile
	doxygen Doxyfile

clean:
	rm -rf calc-area
	rm -rf $(SRC:.cpp=.o)
	rm -rf $(SRC:.cpp=.d)

%.o: %.cpp
	$(CC) -c $(CFLAGS) $*.cpp -o $*.o
	$(CC) -MM $(CFLAGS) $*.cpp > $*.d
	@mv -f $*.d $*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $*.d.tmp > $*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $*.d.tmp | fmt -1 | \
          sed -e 's/^ *//' -e 's/$$/:/' >> $*.d
	@rm -f $*.d.tmp

-include $(SRC:.cpp=.d)

